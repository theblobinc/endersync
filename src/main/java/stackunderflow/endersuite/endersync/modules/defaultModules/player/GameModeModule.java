package stackunderflow.endersuite.endersync.modules.defaultModules.player;

import endersuite.libs.datastore.DataStore;
import endersuite.libs.datastore.abstractions.Column;
import endersuite.libs.datastore.abstractions.Properties;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import stackunderflow.endersuite.endersync.modules.Module;
import stackunderflow.endersuite.endersync.modules.ModuleType;

import java.util.UUID;

/**
 * A module that syncs the players air.
 */
public class GameModeModule extends Module {

// ================================     VARS

    // References
    // Foo

    // Settings
    // foo



    // ================================     CONSTRUCTOR

    public GameModeModule() {

        super("player_gamemode", ModuleType.PLAYERDATA);

        getTableBlueprint().addColumn(new Column("gamemode_value", String.class));

    }



    // ================================     LOGIC


    @Override
    public void generatePlayerRow(Player player) {

        DataStore.getAdapter().set(getTableName(),
                new Properties()
                        .set("_id", UUID.randomUUID().toString())
                        .set("gamemode_value", player.getGameMode().toString())
        );

    }

    @Override
    public void playerSync(Player player, Properties playerData) {

        player.setGameMode(GameMode.valueOf((String) playerData.get("gamemode_value")));

    }

    @Override
    public void playerSave(Player player) {

        DataStore.getAdapter().set(getTableName(),
                new Properties()
                        .set("gamemode_value", player.getGameMode().toString())
        );

    }

}
