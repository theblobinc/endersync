package stackunderflow.endersuite.endersync.threading.runnables;

import endersuite.libs.datastore.adapters.Adapter;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.event.player.PlayerJoinEvent;
import stackunderflow.endersuite.endersync.api.SyncManager;


@Getter
@Setter
public class PlayerJoinRunnable implements Runnable {

    // ================================     VARS

    // References
    public PlayerJoinEvent event;
    private Adapter databaseAdapter;

    // Settings
    // foo



    // ================================     CONSTRUCTOR

    public PlayerJoinRunnable(PlayerJoinEvent event) {

        // Set variables.
        setEvent(event);
        setDatabaseAdapter(null);

    }



    // ================================     LOGIC


    @Override
    public void run() {

        // Start sync of modules.
        SyncManager.syncPlayer(getEvent().getPlayer());

    }

}
