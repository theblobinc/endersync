package stackunderflow.endersuite.endersync.threading;

import endersuite.libs.core.config.ConfigManager;
import endersuite.libs.core.exceptions.DuplicateInstantiationException;
import endersuite.libs.core.utils.StringFormatter;
import lombok.Getter;
import lombok.Setter;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


/**
 * Manages threads and the thread pool.
 */
@Getter
@Setter
public class Threading {

    // ================================     VARS

    // References
    public static Threading INSTANCE;
    private ThreadPoolExecutor threadPool;

    // Settings
    private int minThreadPoolSize = 5;
    private int maxThreadPoolSize = 50;
    private long threadKeepAliveTime = (long) 60.0;



    // ================================     CONSTRUCTOR

    public Threading() {
        if (Threading.INSTANCE == null) Threading.INSTANCE = this;
        else {
            new StringFormatter("{prefix} §cDuplicate instantiation of class \"" + getClass().getSimpleName() + "\"").log("error");
            new StringFormatter("{prefix} Please contact support with this error!").log("info");
            new StringFormatter("").log("info");
            new DuplicateInstantiationException(getClass().getSimpleName()).printStackTrace();
            new StringFormatter("").log("info");
        }

        // Set variables.
        setThreadPool((ThreadPoolExecutor) Executors.newFixedThreadPool(getMaxThreadPoolSize()));
        setMinThreadPoolSize(ConfigManager.get().getInt("plugin.advanced.minThreadPoolSize"));
        setMaxThreadPoolSize(ConfigManager.get().getInt("plugin.advanced.maxThreadPoolSize"));
        setThreadKeepAliveTime(ConfigManager.get().getLong("plugin.advanced.threadKeepAliveTime"));

        // Setup threading.
        getThreadPool().setCorePoolSize(getMinThreadPoolSize());
        //getThreadPool().setMaximumPoolSize(getMaxThreadPoolSize());
        getThreadPool().setKeepAliveTime(getThreadKeepAliveTime(), TimeUnit.SECONDS);

    }



    // ================================     LOGIC


    /**
     * Submits a runnable to the thread pool.
     */
    public static void submit(Runnable runnable) { Threading.INSTANCE.getThreadPool().submit(runnable); }




}