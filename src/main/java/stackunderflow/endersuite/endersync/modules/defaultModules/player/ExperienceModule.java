package stackunderflow.endersuite.endersync.modules.defaultModules.player;

import endersuite.libs.datastore.DataStore;
import endersuite.libs.datastore.abstractions.Column;
import endersuite.libs.datastore.abstractions.Properties;
import org.bukkit.entity.Player;
import stackunderflow.endersuite.endersync.modules.Module;
import stackunderflow.endersuite.endersync.modules.ModuleType;

import java.util.UUID;

/**
 * A module that syncs the players experience.
 */
public class ExperienceModule extends Module {

// ================================     VARS

    // References
    // Foo

    // Settings
    // foo



    // ================================     CONSTRUCTOR

    public ExperienceModule() {

        super("player_experience", ModuleType.PLAYERDATA);

        getTableBlueprint().addColumn(new Column("exp_total_experience", Integer.class));

    }



    // ================================     LOGIC


    @Override
    public void generatePlayerRow(Player player) {

        DataStore.getAdapter().set(getTableName(),
                new Properties()
                        .set("_id", UUID.randomUUID().toString())
                        .set("exp_total_experience", player.getTotalExperience())
        );

    }

    @Override
    public void playerSync(Player player, Properties playerData) {

        player.setExp(0);
        player.setLevel(0);
        player.setTotalExperience(0);
        player.giveExp((Integer) playerData.get("exp_total_experience"));

    }

    @Override
    public void playerSave(Player player) {

        DataStore.getAdapter().set(getTableName(),
                new Properties()
                        .set("exp_total_experience", player.getTotalExperience())
        );

    }

}
