package stackunderflow.endersuite.endersync.modules;

/**
 * The type of the module.
 *
 * PLAYERDATA - The data the module is processing is bound to a specific player.
 * GENERALDATA - The data the module is processing is general.
 *
 */
public enum ModuleType {
    PLAYERDATA,
    GENERALDATA
}
