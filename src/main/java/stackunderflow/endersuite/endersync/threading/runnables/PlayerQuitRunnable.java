package stackunderflow.endersuite.endersync.threading.runnables;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.event.player.PlayerQuitEvent;
import stackunderflow.endersuite.endersync.api.SyncManager;


@Getter
@Setter
public class PlayerQuitRunnable implements Runnable {

    // ================================     VARS

    // References
    public PlayerQuitEvent event;

    // Settings
    // foo



    // ================================     CONSTRUCTOR

    public PlayerQuitRunnable(PlayerQuitEvent event) {

        // Set variables.
        setEvent(event);

    }



    // ================================     LOGIC


    @Override
    public void run() {

        // Start save of player.
        SyncManager.savePlayer(getEvent().getPlayer());

    }

}
