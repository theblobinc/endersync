package stackunderflow.endersuite.endersync.networking.listeners;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import endersuite.libs.core.utils.StringFormatter;
import stackunderflow.endersuite.endersync.networking.NetworkBridge;
import stackunderflow.endersuite.networkbridge.networking.packets.AuthChallengeAcceptedPacket;

public class AuthenticationAcceptedListener implements Listener {

    @Override
    public void received(Connection connection, Object object) {

        // RET: Wrong packet.
        if (!(object instanceof AuthChallengeAcceptedPacket)) return;
        AuthChallengeAcceptedPacket packet = (AuthChallengeAcceptedPacket) object;

        // Display info.
        new StringFormatter("{prefix} §fConnection to §b§l" +
                NetworkBridge.INSTANCE.getHost() + "§f:§b§l" +
                NetworkBridge.INSTANCE.getPort() + " §fwas established!").setSuccess(true).log("info");

    }

}
