package stackunderflow.endersuite.endersync;

import endersuite.libs.core.config.ConfigManager;
import endersuite.libs.core.exceptions.DuplicateInstantiationException;
import endersuite.libs.core.security.Crypto;
import endersuite.libs.core.utils.StringFactory;
import endersuite.libs.core.utils.StringFormatter;
import endersuite.libs.datastore.DataStore;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.java.JavaPlugin;
import stackunderflow.endersuite.endersync.listeners.PlayerJoinListener;
import stackunderflow.endersuite.endersync.listeners.PlayerQuitListener;
import stackunderflow.endersuite.endersync.modules.ModuleManager;
import stackunderflow.endersuite.endersync.networking.NetworkBridge;
import stackunderflow.endersuite.endersync.threading.Threading;

import java.io.IOException;

@Getter
@Setter
public class Main extends JavaPlugin {

    // ================================     VARS

    // References
    public static Main INSTANCE;
    public ConsoleCommandSender console = getServer().getConsoleSender();

    // Settings
    private boolean DEBUG = true;
    private String pluginChatPrefix = "§l§7» §l§3Ender§l§fSync {success}";
    private String version = getDescription().getVersion();
    public boolean vault_sync_enable = false;



    // ================================     CONSTRUCTOR & SINGLETON

    public Main() {
        if (Main.INSTANCE == null) Main.INSTANCE = this;
        else {
            new StringFormatter("{prefix} §cDuplicate instantiation of class \""+ getClass().getSimpleName() +"\"").log("error");
            new StringFormatter("{prefix} Please contact support with this error!").log("info");
            new StringFormatter("").log("info");
            new DuplicateInstantiationException(getClass().getSimpleName()).printStackTrace();
            new StringFormatter("").log("info");
        }
    }



    // ================================     BUKKIT OVERRIDES

    @Override
    public void onEnable() {

        new StringFactory();
        StringFactory.setDEBUG(isDEBUG());
        StringFactory.setPluginChatPrefix(getPluginChatPrefix());

        new StringFormatter("").log("info");
        new StringFormatter("{prefix} §7=========[ §b§lEnder§f§lSync §7§lv§f§l" + getVersion() + " §7]=========").log("info");
        new StringFormatter("{prefix} §fLoading configuration...").log("info");

        ConfigManager.load("config.yml");
        ConfigManager.load("features.yml");

        String languageFileName = "lang-" + ConfigManager.get().getString("plugin.lang") + ".yml";
        ConfigManager.load(languageFileName);
        ConfigManager.setLanguageFile(languageFileName);

        // Update settings.
        setDEBUG(ConfigManager.get().getBoolean("plugin.debug"));
        setPluginChatPrefix(ConfigManager.get().getString("plugin.chatPrefix"));
        StringFactory.setDEBUG(isDEBUG());
        StringFactory.setPluginChatPrefix(getPluginChatPrefix());

        new StringFormatter("{prefix} §fInitializing data store \"§b"+ ConfigManager.get().getString("dataStore.adapter") +"§f\"...").log("info");

        new DataStore();
        DataStore.INSTANCE.init(
                ConfigManager.get().getString("dataStore.adapter"),
                ConfigManager.get().getString("dataStore.host"),
                ConfigManager.get().getInt("dataStore.port"),
                ConfigManager.get().getString("dataStore.user"),
                ConfigManager.get().getString("dataStore.password"),
                ConfigManager.get().getString("dataStore.databaseName")
        );

        new StringFormatter("{prefix} §fInitializing crypto...").log("info");

        new Crypto();
        Crypto.INSTANCE.setKEY(ConfigManager.get().getString("networking.key").getBytes());

        new StringFormatter("{prefix} §fInitializing network bridge...").log("info");

        new NetworkBridge();
        try {
            NetworkBridge.INSTANCE.connect();
        } catch (IOException e) {

            new StringFormatter("").log("info");
            new StringFormatter("{prefix} §cCould not connect to server!").log("error");
            e.printStackTrace();
            new StringFormatter("").log("info");

        }

        new StringFormatter("{prefix} §fInitializing plugin...").log("info");

        // Enable threading.
        new Threading();

        // Register commands.

        // Register listeners.
        getServer().getPluginManager().registerEvents(new PlayerJoinListener(), this);
        getServer().getPluginManager().registerEvents(new PlayerQuitListener(), this);

        new StringFormatter("{prefix} §fLoading modules...").log("info");

        new ModuleManager();
        ModuleManager.loadDefaultModules();

        new StringFormatter("{prefix} §fPlugin is Ready! :-)").setSuccess(true).log("info");

        new StringFormatter("").log("info");

    }


    @Override
    public void onDisable() {

        new StringFormatter("{prefix} Disabling §b§lEnder§f§lSync §7§lv§f§l" + getVersion() + "§7...").log("info");

        try {
            NetworkBridge.INSTANCE.disconnect();
        } catch (IOException e) {

            new StringFormatter("").log("info");
            new StringFormatter("{prefix} §cCould not close connection at shutdown!").log("error");
            e.printStackTrace();
            new StringFormatter("").log("info");

        }

    }

}
