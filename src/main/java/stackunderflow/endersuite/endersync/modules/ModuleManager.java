package stackunderflow.endersuite.endersync.modules;


import endersuite.libs.core.exceptions.DuplicateInstantiationException;
import endersuite.libs.core.utils.StringFormatter;
import lombok.Getter;
import lombok.Setter;
import stackunderflow.endersuite.endersync.modules.defaultModules.player.*;

import java.util.HashMap;


/**
 * Manages modules.
 */
@Getter
@Setter
public class ModuleManager {

    // ================================     VARS

    // References
    public static ModuleManager INSTANCE;
    private HashMap<String, Module> loadedModules;

    // Settings
    // foo



    // ================================     CONSTRUCTOR

    public ModuleManager() {
        if (ModuleManager.INSTANCE == null) ModuleManager.INSTANCE = this;
        else {
            new StringFormatter("{prefix} §cDuplicate instantiation of class \"" + getClass().getSimpleName() + "\"").log("error");
            new StringFormatter("{prefix} Please contact support with this error!").log("info");
            new StringFormatter("").log("info");
            new DuplicateInstantiationException(getClass().getSimpleName()).printStackTrace();
            new StringFormatter("").log("info");
        }

        // Set variables.
        setLoadedModules(new HashMap<>());

    }



    // ================================     LOGIC


    /**
     * Loads & Enables a module.
     *
     * @param module The module instance to enable & load / initialize.
     */
    public static void loadModule(Module module) {

        new StringFormatter("{prefix}   §f+ §b" + module.getName()).log("debug");

        // Add module to map.
        ModuleManager.INSTANCE.getLoadedModules().put(module.getName(), module);

        // Initialize module.
        ModuleManager.INSTANCE.getLoadedModules().get(module.getName()).init();

    }


    /**
     * Disables a module.
     *
     * @param name The name of the module.
     */
    public static void disableModule(String name) {

        // Remove form map.
        ModuleManager.INSTANCE.getLoadedModules().remove(name);

    }


    /**
     * Loads all default modules.
     */
    public static void loadDefaultModules() {

        ModuleManager.loadModule(new LocationModule());
        ModuleManager.loadModule(new HealthModule());
        ModuleManager.loadModule(new AirModule());
        ModuleManager.loadModule(new ExperienceModule());
        ModuleManager.loadModule(new FoodModule());
        ModuleManager.loadModule(new FlightModule());
        ModuleManager.loadModule(new GameModeModule());

    }



}
