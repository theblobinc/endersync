package stackunderflow.endersuite.endersync.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import stackunderflow.endersuite.endersync.threading.runnables.PlayerJoinRunnable;
import stackunderflow.endersuite.endersync.threading.Threading;

public class PlayerJoinListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerJoin(PlayerJoinEvent event) {

        // Create thread.
        Threading.submit(new PlayerJoinRunnable(event));

    }

}
