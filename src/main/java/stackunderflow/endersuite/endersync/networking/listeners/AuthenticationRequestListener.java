package stackunderflow.endersuite.endersync.networking.listeners;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import endersuite.libs.core.security.Crypto;
import endersuite.libs.core.utils.StringFormatter;
import stackunderflow.endersuite.endersync.networking.NetworkBridge;
import stackunderflow.endersuite.networkbridge.networking.packets.AuthChallengeRequestPacket;
import stackunderflow.endersuite.networkbridge.networking.packets.AuthChallengeResponsePacket;

public class AuthenticationRequestListener implements Listener {

    @Override
    public void received(Connection connection, Object object) {

        // RET: Wrong packet.
        if (!(object instanceof AuthChallengeRequestPacket)) return;
        AuthChallengeRequestPacket packet = (AuthChallengeRequestPacket) object;

        // Return response with signed challenge.
        AuthChallengeResponsePacket responsePacket = new AuthChallengeResponsePacket();
        responsePacket.setClientName(NetworkBridge.INSTANCE.getClientName());
        responsePacket.setClientServerVersion(NetworkBridge.INSTANCE.getClientServerVersion());
        try {
            responsePacket.setChallenge(Crypto.INSTANCE.encrypt(packet.getChallenge()));
        } catch (Exception e) {

            // USed to properly handle the server side response.
            responsePacket.setChallenge("null");

            new StringFormatter("").log("info");
            new StringFormatter("{prefix} §cError whilst encrypting challenge string!").log("error");
            new StringFormatter("{prefix} Please contact support with this error!").log("info");
            e.printStackTrace();
            new StringFormatter("").log("info");

        }
        connection.sendTCP(responsePacket);

    }

}
