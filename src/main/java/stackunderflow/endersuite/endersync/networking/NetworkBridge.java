package stackunderflow.endersuite.endersync.networking;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.minlog.Log;
import endersuite.libs.core.config.ConfigManager;
import endersuite.libs.core.exceptions.DuplicateInstantiationException;
import endersuite.libs.core.utils.StringFormatter;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import stackunderflow.endersuite.endersync.networking.listeners.AuthenticationAcceptedListener;
import stackunderflow.endersuite.endersync.networking.listeners.AuthenticationRejectedListener;
import stackunderflow.endersuite.endersync.networking.listeners.AuthenticationRequestListener;
import stackunderflow.endersuite.networkbridge.networking.packets.AuthChallengeAcceptedPacket;
import stackunderflow.endersuite.networkbridge.networking.packets.AuthChallengeRejectedPacket;
import stackunderflow.endersuite.networkbridge.networking.packets.AuthChallengeRequestPacket;
import stackunderflow.endersuite.networkbridge.networking.packets.AuthChallengeResponsePacket;

import java.io.IOException;


/*
 * Handles networking between instances.
 */
@Getter
@Setter
public class NetworkBridge {

    // ================================     VARS

    // References
    public static NetworkBridge INSTANCE;
    private Client client;

    // Settings
    private String clientName = "null";
    private String clientServerVersion = "0.0.0";
    private String host = "127.0.0.1";
    private int port = 20344;



    // ================================     CONSTRUCTOR & SINGLETON

    public NetworkBridge() {
        if (NetworkBridge.INSTANCE == null) NetworkBridge.INSTANCE = this;
        else {
            new StringFormatter("{prefix} §cDuplicate instantiation of class \""+ getClass().getSimpleName() +"\"").log("error");
            new StringFormatter("{prefix} Please contact support with this error!").log("info");
            new StringFormatter("").log("info");
            new DuplicateInstantiationException(getClass().getSimpleName()).printStackTrace();
            new StringFormatter("").log("info");
        }

        // Configure networking settings.
        setClientName(ConfigManager.get().getString("networking.clientName"));
        setClientServerVersion(Bukkit.getBukkitVersion());
        setHost(ConfigManager.get().getString("networking.bridgeHost"));
        setPort(ConfigManager.get().getInt("networking.bridgePort"));

        // Configure KryoNet.
        setClient(new Client());
        Log.set(Log.LEVEL_ERROR);

        registerPackets();
        registerListeners();

        getClient().start();

    }



    // ================================     LOGIC

    /*
     * Connects to the configured server.
     */
    public void connect() throws IOException {
        getClient().connect(5000, getHost(), getPort());
    }


    /*
     * Stops any connection and the client.
     */
    public void disconnect() throws IOException {
        getClient().close();
        getClient().stop();
    }



    // ================================     HELPERS

    /*
     * Registers network packets.
     */
    private void registerPackets() {

        getClient().getKryo().register(AuthChallengeRequestPacket.class);
        getClient().getKryo().register(AuthChallengeResponsePacket.class);
        getClient().getKryo().register(AuthChallengeRejectedPacket.class);
        getClient().getKryo().register(AuthChallengeAcceptedPacket.class);

    }

    /*
     * Registers network packet listeners.
     */
    private void registerListeners() {

        getClient().addListener(new AuthenticationRequestListener());
        getClient().addListener(new AuthenticationRejectedListener());
        getClient().addListener(new AuthenticationAcceptedListener());

    }

}
