package stackunderflow.endersuite.endersync.networking.listeners;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import endersuite.libs.core.utils.StringFormatter;
import stackunderflow.endersuite.endersync.networking.NetworkBridge;
import stackunderflow.endersuite.networkbridge.networking.packets.AuthChallengeRejectedPacket;

public class AuthenticationRejectedListener implements Listener {

    @Override
    public void received(Connection connection, Object object) {

        // RET: Wrong packet.
        if (!(object instanceof AuthChallengeRejectedPacket)) return;
        AuthChallengeRejectedPacket packet = (AuthChallengeRejectedPacket) object;

        // Display error.
        new StringFormatter("").log("info");
        new StringFormatter("{prefix} §cConnection to §b§l" +
                NetworkBridge.INSTANCE.getHost() + "§f:§b§l" +
                NetworkBridge.INSTANCE.getPort() + " §cwas rejected:").log("error");
        new StringFormatter("{prefix} " + packet.getMessage()).log("error");
        new StringFormatter("").log("info");

    }

}
