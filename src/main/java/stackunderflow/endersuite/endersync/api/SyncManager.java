package stackunderflow.endersuite.endersync.api;

import endersuite.libs.core.exceptions.DuplicateInstantiationException;
import endersuite.libs.core.utils.StringFormatter;
import org.bukkit.entity.Player;
import stackunderflow.endersuite.endersync.modules.Module;
import stackunderflow.endersuite.endersync.modules.ModuleManager;
import stackunderflow.endersuite.endersync.modules.ModuleType;


public class SyncManager {

    // ================================     VARS

    // References
    private static SyncManager INSTANCE;

    // Settings
    // Foo



    // ================================     CONSTRUCTOR

    public SyncManager() {
        if (SyncManager.INSTANCE == null) SyncManager.INSTANCE = this;
        else {
            new StringFormatter("{prefix} §cDuplicate instantiation of class \"" + getClass().getSimpleName() + "\"").log("error");
            new StringFormatter("{prefix} Please contact support with this error!").log("info");
            new StringFormatter("").log("info");
            new DuplicateInstantiationException(getClass().getSimpleName()).printStackTrace();
            new StringFormatter("").log("info");
        }

        // Set variables.
        // Foo

    }



    // ================================     LOGIC


    /**
     * Syncs a player.
     * This method also calculates, which modules should be synced
     * due to the player world and other settings.
     *
     * @param player The player to sync.
     */
    public static void syncPlayer(Player player) {

        for (Module module : ModuleManager.INSTANCE.getLoadedModules().values()) {

            // RET: Module does not sync PlayerData.
            if (!module.getType().equals(ModuleType.PLAYERDATA)) continue;

            // Sync the module.
            module._playerSync(player);

        }

    }

    /**
     * Saves a player.
     * This method also calculates, which modules should be saved
     * due to the player world and other settings.
     *
     * @param player The player to save.
     */
    public static void savePlayer(Player player) {

        for (Module module : ModuleManager.INSTANCE.getLoadedModules().values()) {

            // RET: Module does not save PlayerData.
            if (!module.getType().equals(ModuleType.PLAYERDATA)) continue;

            // Save the module.
            module._playerSave(player);

        }

    }


}
