package stackunderflow.endersuite.endersync.modules.defaultModules.player;

import endersuite.libs.datastore.DataStore;
import endersuite.libs.datastore.abstractions.Column;
import endersuite.libs.datastore.abstractions.Properties;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import stackunderflow.endersuite.endersync.modules.Module;
import stackunderflow.endersuite.endersync.modules.ModuleType;

import java.util.UUID;

/**
 * A module that syncs the players health.
 */
public class HealthModule extends Module {

// ================================     VARS

    // References
    // Foo

    // Settings
    // foo



    // ================================     CONSTRUCTOR

    public HealthModule() {

        super("player_health", ModuleType.PLAYERDATA);

        getTableBlueprint().addColumn(new Column("health_value", Double.class));
        getTableBlueprint().addColumn(new Column("health_scale", Double.class));
        getTableBlueprint().addColumn(new Column("health_max", Double.class));

    }



    // ================================     LOGIC


    @Override
    public void generatePlayerRow(Player player) {

        DataStore.getAdapter().set(getTableName(),
                new Properties()
                        .set("_id", UUID.randomUUID().toString())
                        .set("health_value", player.getHealth())
                        .set("health_scale", player.getHealthScale())
                        .set("health_max", player.getMaxHealth())
        );

    }

    @Override
    public void playerSync(Player player, Properties playerData) {

        player.setHealth((Double) playerData.get("health_value"));
        player.setHealthScale((Double) playerData.get("health_scale"));
        player.setMaxHealth((Double) playerData.get("health_max"));

    }

    @Override
    public void playerSave(Player player) {

        DataStore.getAdapter().set(getTableName(),
                new Properties()
                        .set("health_value", player.getHealth())
                        .set("health_scale", player.getHealthScale())
                        .set("health_max", player.getMaxHealth())
        );

    }

}
