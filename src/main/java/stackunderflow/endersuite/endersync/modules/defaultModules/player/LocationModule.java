package stackunderflow.endersuite.endersync.modules.defaultModules.player;

import endersuite.libs.datastore.DataStore;
import endersuite.libs.datastore.abstractions.Column;
import endersuite.libs.datastore.abstractions.Properties;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import stackunderflow.endersuite.endersync.modules.Module;
import stackunderflow.endersuite.endersync.modules.ModuleType;

import java.util.UUID;

/**
 * A module that syncs the players location and viewing angle.
 */
public class LocationModule extends Module {

// ================================     VARS

    // References
    // Foo

    // Settings
    // foo



    // ================================     CONSTRUCTOR

    public LocationModule() {

        super("player_location", ModuleType.PLAYERDATA);

        getTableBlueprint().addColumn(new Column("world_name", String.class));
        getTableBlueprint().addColumn(new Column("pos_x", Double.class));
        getTableBlueprint().addColumn(new Column("pos_y", Double.class));
        getTableBlueprint().addColumn(new Column("pos_z", Double.class));
        getTableBlueprint().addColumn(new Column("yaw", Float.class));
        getTableBlueprint().addColumn(new Column("pitch", Float.class));

    }



    // ================================     LOGIC


    @Override
    public void generatePlayerRow(Player player) {

        DataStore.getAdapter().set(getTableName(),
                new Properties()
                        .set("_id", UUID.randomUUID().toString())
                        .set("player_uuid", player.getUniqueId().toString())
                        .set("world_name", player.getWorld().getName())
                        .set("pos_x", player.getLocation().getX())
                        .set("pos_y", player.getLocation().getY())
                        .set("pos_z", player.getLocation().getZ())
                        .set("yaw", player.getLocation().getYaw())
                        .set("pitch", player.getLocation().getPitch())
        );

    }

    @Override
    public void playerSync(Player player, Properties playerData) {

        player.teleport(new Location(
                Bukkit.getWorld((String) playerData.get("world_name")),
                (Double) playerData.get("pos_x"),
                (Double) playerData.get("pos_y"),
                (Double) playerData.get("pos_z"),
                (Float) playerData.get("yaw"),
                (Float) playerData.get("pitch")
        ));

    }

    @Override
    public void playerSave(Player player) {

        DataStore.getAdapter().set(getTableName(),
                new Properties()
                        .set("player_uuid", player.getUniqueId().toString())
                        .set("world_name", player.getWorld().getName())
                        .set("pos_x", player.getLocation().getX())
                        .set("pos_y", player.getLocation().getY())
                        .set("pos_z", player.getLocation().getZ())
                        .set("yaw", player.getLocation().getYaw())
                        .set("pitch", player.getLocation().getPitch())
        );

    }

}
