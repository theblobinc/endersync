package stackunderflow.endersuite.endersync.modules.defaultModules.player;

import endersuite.libs.datastore.DataStore;
import endersuite.libs.datastore.abstractions.Column;
import endersuite.libs.datastore.abstractions.Properties;
import org.bukkit.entity.Player;
import stackunderflow.endersuite.endersync.modules.Module;
import stackunderflow.endersuite.endersync.modules.ModuleType;

import java.util.UUID;

/**
 * A module that syncs the players air.
 */
public class FoodModule extends Module {

// ================================     VARS

    // References
    // Foo

    // Settings
    // foo



    // ================================     CONSTRUCTOR

    public FoodModule() {

        super("player_food", ModuleType.PLAYERDATA);

        getTableBlueprint().addColumn(new Column("food_value", Integer.class));

    }



    // ================================     LOGIC


    @Override
    public void generatePlayerRow(Player player) {

        DataStore.getAdapter().set(getTableName(),
                new Properties()
                        .set("_id", UUID.randomUUID().toString())
                        .set("food_value", player.getFoodLevel())
        );

    }

    @Override
    public void playerSync(Player player, Properties playerData) {

        player.setFoodLevel((Integer) playerData.get("food_value"));

    }

    @Override
    public void playerSave(Player player) {

        DataStore.getAdapter().set(getTableName(),
                new Properties()
                        .set("food_value", player.getFoodLevel())
        );

    }

}
