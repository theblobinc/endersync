package stackunderflow.endersuite.endersync.modules;

import endersuite.libs.datastore.DataStore;
import endersuite.libs.datastore.abstractions.Column;
import endersuite.libs.datastore.abstractions.Properties;
import endersuite.libs.datastore.abstractions.TableBlueprint;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.Player;

import java.util.UUID;


/**
 * Represents a module for the save / sync process.
 */
@Getter
@Setter
public class Module {

    // ================================     VARS

    // References
    private TableBlueprint tableBlueprint;

    // Settings
    private boolean valid = false;
    private String name = "null";
    private String tableName = "null";
    private ModuleType type = null;



    // ================================     CONSTRUCTOR

    public Module(String name, ModuleType type) {

        // Set variables.
        setName(name);
        setTableName("es_module_" + getName());
        setType(type);

        // Initialize TableBlueprint.
        setTableBlueprint(new TableBlueprint(getTableName()));

        // Add player_uuid column if module stores PlayerData.
        if (type.equals(ModuleType.PLAYERDATA)) getTableBlueprint().addColumn(new Column("player_uuid", UUID.class).makeUnique().makeNotNull().makePrimaryKey());
        else {
            // Add _id column for system.
            getTableBlueprint().addColumn(
                    new Column("_id", UUID.class)
                            .makeNotNull()
                            .makeUnique()
                            .makePrimaryKey()
            );
        }


    }



    // ================================     LOGIC

    /**
     * Initializes the module on enable.
     * This means creating the tables / updating them.
     */
    public void init() {

        // Ensure the database table exists.
        getTableBlueprint().create();

        // TODO: Add error valid= false & disable

    }


    /**
     * Prepares for a sync.
     *
     * @param player The player to sync.
     */
    public void _playerSync(Player player) {

        // Get row from database.
        Properties playerData = DataStore.getAdapter().getOne("SELECT * FROM " + getTableName() + " WHERE player_uuid='{player_uuid}'",
                new Properties()
                        .set("player_uuid", player.getUniqueId().toString())
        );

        // No data found.
        if (!playerData.containsKey("player_uuid")) generatePlayerRow(player);

        // Call the actual custom sync logic.
        playerSync(player, playerData);

    }


    /**
     * Prepares for a save.
     *
     * @param player The player to save.
     */
    public void _playerSave(Player player) {
        playerSave(player);
    }



    // ================================     OVERRIDES


    /**
     * Is called when there is no data found.
     * Override with logic to insert new row with current data.
     *
     * @param player The player associated.
     */
    public void generatePlayerRow(Player player) {}


    /**
     * Is called when a player should be synced.
     *
     * @param player The player to sync.
     * @param playerData The data from database.
     */
    public void playerSync(Player player, Properties playerData) {}


    /**
     * Is called when a player should be saved.
     *
     * @param player The player to sync.
     */
    public void playerSave(Player player) {}

}
