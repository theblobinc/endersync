package stackunderflow.endersuite.endersync.modules.defaultModules.player;

import endersuite.libs.datastore.DataStore;
import endersuite.libs.datastore.abstractions.Column;
import endersuite.libs.datastore.abstractions.Properties;
import org.bukkit.entity.Player;
import stackunderflow.endersuite.endersync.modules.Module;
import stackunderflow.endersuite.endersync.modules.ModuleType;

import java.util.UUID;

/**
 * A module that syncs the players air.
 */
public class AirModule extends Module {

// ================================     VARS

    // References
    // Foo

    // Settings
    // foo



    // ================================     CONSTRUCTOR

    public AirModule() {

        super("player_air", ModuleType.PLAYERDATA);

        getTableBlueprint().addColumn(new Column("air_max", Integer.class));
        getTableBlueprint().addColumn(new Column("air_level", Integer.class));

    }



    // ================================     LOGIC


    @Override
    public void generatePlayerRow(Player player) {

        DataStore.getAdapter().set(getTableName(),
                new Properties()
                        .set("_id", UUID.randomUUID().toString())
                        .set("air_max", player.getMaximumAir())
                        .set("air_level", player.getRemainingAir())
        );

    }

    @Override
    public void playerSync(Player player, Properties playerData) {

        player.setMaximumAir((Integer) playerData.get("air_max"));
        player.setRemainingAir((Integer) playerData.get("air_level"));

    }

    @Override
    public void playerSave(Player player) {

        DataStore.getAdapter().set(getTableName(),
                new Properties()
                        .set("air_max", player.getMaximumAir())
                        .set("air_level", player.getRemainingAir())
        );

    }

}
