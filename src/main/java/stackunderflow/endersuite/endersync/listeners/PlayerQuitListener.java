package stackunderflow.endersuite.endersync.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import stackunderflow.endersuite.endersync.threading.Threading;
import stackunderflow.endersuite.endersync.threading.runnables.PlayerQuitRunnable;

public class PlayerQuitListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerQuit(PlayerQuitEvent event) {

        // Create thread.
        Threading.submit(new PlayerQuitRunnable(event));

    }

}
