package stackunderflow.endersuite.endersync.modules.defaultModules.player;

import endersuite.libs.datastore.DataStore;
import endersuite.libs.datastore.abstractions.Column;
import endersuite.libs.datastore.abstractions.Properties;
import org.bukkit.entity.Player;
import stackunderflow.endersuite.endersync.modules.Module;
import stackunderflow.endersuite.endersync.modules.ModuleType;

import java.util.UUID;

/**
 * A module that syncs the players air.
 */
public class FlightModule extends Module {

// ================================     VARS

    // References
    // Foo

    // Settings
    // foo



    // ================================     CONSTRUCTOR

    public FlightModule() {

        super("player_flight", ModuleType.PLAYERDATA);

        getTableBlueprint().addColumn(new Column("allow_flying", Boolean.class));
        getTableBlueprint().addColumn(new Column("flying_speed", Float.class));
        getTableBlueprint().addColumn(new Column("is_flying", Boolean.class));

    }



    // ================================     LOGIC


    @Override
    public void generatePlayerRow(Player player) {

        DataStore.getAdapter().set(getTableName(),
                new Properties()
                        .set("_id", UUID.randomUUID().toString())
                        .set("allow_flying", player.getAllowFlight())
                        .set("flying_speed", player.getFlySpeed())
                        .set("is_flying", player.isFlying())
        );

    }

    @Override
    public void playerSync(Player player, Properties playerData) {

        player.setAllowFlight((Boolean) playerData.get("allow_flying"));
        player.setFlySpeed((Integer) playerData.get("flying_speed"));
        player.setFlying((Boolean) playerData.get("is_flying"));

    }

    @Override
    public void playerSave(Player player) {

        DataStore.getAdapter().set(getTableName(),
                new Properties()
                        .set("allow_flying", player.getAllowFlight())
                        .set("flying_speed", player.getFlySpeed())
                        .set("is_flying", player.isFlying())
        );

    }

}
